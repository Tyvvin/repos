 #!/bin/bash 

# get headers containing location
string_code=$(curl --silent -I -u Tyvvin:google23 \
"https://bitbucket.org/site/oauth2/authorize?client_id=bRPmuFaFmKGyQcuC6F&response_type=code" \
| grep "Location:" \
| awk -F '/?code=' '{print $2}' \
| tr -d '\r') 

# get new access_token
string_token=$(curl --silent -X POST -u "bRPmuFaFmKGyQcuC6F:JrKyMmPy83xEmRnCVU3KxhHt5qNrFK8e" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=authorization_code -d code=$string_code \
 | grep "access_token" \
 | awk -F '"' '{print $4}')

 #get info about our repositories 
 curl -H "Authorization: Bearer $string_token" https://bitbucket.org/api/2.0/repositories/Tyvvin/repos

